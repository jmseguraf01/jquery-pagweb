$(document).ready(function(){

  var random_width = 100;
  var random_height = 100;

  // For para añadir 10 divs de circulos
  for (var i = 0; i < 10; i++) {
    $("body").append("<div></div>");
  };

  // Funcion que coje cada div y le aplica lo de dentro de la Funcion PARA CADA UNO
  // Es como si fuera un bucle pero con una funcion
  $("div").each(function(){
    var random_width = Math.floor((Math.random() * 500) + 100);
    var random_height = Math.floor((Math.random() * 500) + 100);
    $(this).css({"border": "thin solid black", "border-radius": "50px", "width": random_width, "height":  random_height, "position": "absolute"});
  });

  // Cuando haces click en todo el html se recarga la pagina
  $("html").click(function(){
    location.reload();
  });

});
