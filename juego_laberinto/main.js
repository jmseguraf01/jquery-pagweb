$(document).ready(function(){
  // Funcion para que cuando han click al boton, se limpie la pantalla y se ejecue el juego
  $("#boton").click(function(){
    $("#title").css("animation", "exit 1s linear 1 forwards");
    // Para hacer desaparecer el boton
    setTimeout('$("#boton").css("display", "none")', 1500);
    // Para que aparezca la pantalla de game
    setTimeout('$("#game").css("display", "block")', 1500);
    // Llamo a la funcion game después de 1500 milisegundos
    setTimeout(game, 1500);

    // Creo la funcion del juego
    function game(){
      $(".div_inside_game:first-child").append("<img id='game_ball' src='img/pelota.png'/>");
      move_ball(); // Llamo a la funcion que hace que se mueva la bola
    };
  });

  /*
  -----------------
        MAPA
  -----------------
  1 --> agua
  2 --> ladrillo
  3 --> camino
  4 --> roca
  5 -- > pelota
  */
  var mapa1 = [
    [3, 2, 3, 3, 3, 3, 1, 2, 4, 5, 4, 3, 3],
    [3, 4, 2, 2, 3, 3, 4, 5, 5, 3, 2, 1, 1],
    [3, 3, 3, 3, 5, 5, 3, 4, 1, 1, 2, 2, 3],
    [4, 4, 2, 2, 3, 3, 4, 1, 1, 5, 4, 3, 3],
    [1, 1, 2, 4, 3, 3, 3, 3, 3, 5, 4, 1, 2],
    [1, 2, 3, 3, 1, 4, 2, 2, 3, 3, 3, 4, 5],
    [5, 5, 3, 3, 1, 2, 2, 5, 3, 1, 5, 4, 2],
    [3, 5, 2, 1, 3, 3, 4, 2, 5, 4, 3, 2, 1],
    [4, 2, 3, 3, 4, 2, 1, 1, 3, 3, 3, 4, 1],
    [3, 3, 3, 3, 5, 5, 3, 4, 1, 1, 2, 2, 3],
    [3, 3, 3, 3, 5, 5, 3, 4, 1, 1, 2, 2, 3],
    [3, 3, 3, 3, 5, 5, 3, 4, 1, 1, 2, 2, 3],
    [3, 3, 3, 3, 5, 5, 3, 4, 1, 1, 2, 2, 3],
    [3, 3, 3, 3, 5, 5, 3, 4, 1, 1, 2, 2, 3],
    [3, 3, 3, 3, 5, 5, 3, 4, 1, 1, 2, 2, 3],
    [3, 3, 3, 3, 5, 5, 3, 4, 1, 1, 2, 2, 3]
  ];

  // A la funcon de create_map le paso el mapa1
  create_map(mapa1);

  function create_map(mapa){

    // Lee las filas del mapa
    var filas = mapa.length;
    for (var i = 0; i < filas; i++) {
      // Le la columna de esa fila
      var columnas = mapa[i].length;
      for (var j = 0; j < columnas; j++) {

        // Le digo lo que es depende del numero que sea, y le añado la clase y el div correspondiente
        var object = "";
        // AGUA
        if (mapa[i][j] == 1) {
          $("#game").append("<div class='div_inside_game_water'></div>");
          object = "agua";

        }
        // LADRILLO
        else if (mapa[i][j] == 2) {
          $("#game").append("<div class='div_inside_game_brick'></div>");
          object = "ladrillo";
        }
        // CAMINO
        else if (mapa[i][j] == 3) {
          $("#game").append("<div class='div_inside_game'></div>");
          object = "camino";
        }

        // ROCA
        else if (mapa[i][j] == 4){
          $("#game").append("<div class='div_inside_game_rock'></div>");
          object = "roca";
        }
        // PELOTA
        else if (mapa[i][j] == 5) {
          $("#game").append("<div class='div_inside_game_ball'></div>");
          object = "pelota";
        }
      };
    };
  };

  // Esta funcion hace que la pelota se mueva
  function move_ball(){
    // Capturamos las teclas que pulsamos ( se controla con las flechas )
    $("html").keydown(function(event){
      // Cojo las posiciones de la pelota
      position_top=$("#game_ball").position().top;
      console.log("Top es: "+ position_top +" ");

      position_left=$("#game_ball").position().left;
      console.log("Left es: "+ position_left +" ");

      // La variable que le voy a sumar o restar la posicion de la pelota
      position_more = 10;
      console.log($("#game_ball").find(".div_inside_game_ball").length > 0);
      if ($("#game_ball").find(".div_inside_game_ball").length > 0){
          alert("encimabrp");
        }


      // Arriba
      if (event.which == 87 ){
        var arriba = position_top-position_more;
        // console.log("Arriba deberia ser:  "+ arriba +" ");
        $("#game_ball").css("top", arriba);
      }

      // Abajo
      if (event.which == 83 ){
        var abajo = position_top+position_more;
        // console.log("Abajod deberia ser: "+ abajo +" ");
        $("#game_ball").css("top", abajo);
      }

      // Derecha
      if (event.which == 68 ){
        var derecha = position_left+position_more;
        // console.log("Derecha deberia ser: "+ derecha +" ");
        $("#game_ball").css("left", derecha);
      }

      // Izquierda
      if (event.which == 65 ){
        var izquierda = position_left-position_more;
        // console.log("Izquierda deberia ser: "+ izquierda +" ");
        $("#game_ball").css("left", izquierda);
      }

    });

  };

});
