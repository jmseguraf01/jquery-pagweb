$(document).ready(function(){  // Para llamar a Jquery

// Para que al recargar la pagina se borre el value de entry
$(".entry").val("");

// Para que salga el value del boton y de boton_operaciones en el input del html
$( ".button, .button_operaciones").click(function(){  // Cuando se haga click en el class button se ejecuta lo siguiente
  var value_anterior = $(".entry").val();  // Le asigno a la variable, el valor que ya tenia el input

  // Si le damos al simbolo de calcular ( CALCULAR )
  if ($(this).val() == "=")  {

    // Si no hay error
    try {
      var resultado = eval(value_anterior); // En la variable resultado se ejecuta la operacion que se ha marcado en value_anterior
      $(".entry").val(resultado);
    }

    // Si hay algun error al calcular el resultado
    catch(err) {
      $(".entry").val("Error");
    }
  }

  // Si le damos al boton de borrar todo ( BORRAR TODO )
  else if ($(this).val() == "borrar_todo") {
    $(".entry").val("");
  }

  // Si le damos al boton de borrar uno ( BORRAR UNO )
  else if ($(this).val() == "borrar_uno") {
    // Defino una variable que le quita 1 numero a value_anterior
    var menos_uno = value_anterior.substring(0, value_anterior.length-1);
    // Pongo la variable en entry
    $(".entry").val(menos_uno);
  }

  // Si le damos a otro numero que no sea el simbolo = ( NUMERO CUALQUIERA )
  else {

    // Si poner Error, ejecuto esta funcion para que se borre "Error" y se ponga el value del boton al que le hago click
    if  ($(".entry").val() == "Error") {
      $(".entry").val($(this).val());
    }

    // Si en el value de .entry no pone error ( LO NORMAL )
    else{
      $(".entry").val(value_anterior + $(this).val()); // Le añado al valor que tenia el input, el valor del value que tiene el boton que he clicado
    }
  }

});


}); // Cierro el jquery
