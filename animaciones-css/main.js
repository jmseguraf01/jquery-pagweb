
  // Meto el alert fuera del document ready porque quiero que salga cuando aun no se ha cargado el html
  // alert("Redimensione la página para que se vean correctamente las animaciones.");

$(document).ready(function(){
  //  MARIO BROS
  // Le año una animacion para que vaya desapareciendo poco a poco cuando entra en el tubo
  setTimeout('$("#imagen_mario").css("animation", "desaperecer_mario 0.2s 1, moverse 0.5s steps(5) infinite, andar 3s steps(1000) 1")', 2800);
  setTimeout('$("#imagen_mario").css("opacity", "0")', 3000);  // Hago que desaparezca
  setTimeout('$("#imagen_mario").css("opacity", "1")', 4000);  // Hago que aparezca en el tubo2
  setTimeout('$("#imagen_mario").css("margin-left", "1800px")', 4000);  // Hago que se mueva a la posicion del tubo
  setTimeout('$("#imagen_mario").css("animation", "mario-subo-tubo2 1.2s 1")', 4000); // Le añado la animacion de que suba por el tubo
  setTimeout('$("#imagen_mario").css("animation", "mario-salir-tubo2 1s 1")', 5200); // Le añado la animacion de salir del tubo y vaya alante
  setTimeout('$("#imagen_mario").css("margin-left", "2100px")', 5700);  // Le pongo la posicion del mario para que se quede fija despues de la animacion
  setTimeout('$("#imagen_mario").css("animation", "moverse 0.5s steps(5) infinite")', 5700);  // Le añado la animacion de hacer que ande

  // GOOMBA
  setTimeout('$("#goomba").css("display", "block")', 4500)  // Hago visible al muñeco de goomba
  setTimeout('$("#goomba").css("animation", "moverse_goomba 5s")', 4500)  // Le añado las animaciones

  // MARIO
  setTimeout('$("#imagen_mario").css("animation", "saltar_mario 1s")', 7000)  // Le añado el salto al mario
  setTimeout('$("#imagen_mario").css("animation", "bajar_mario 1.5s")', 8000)   // Le añado la animacion de que baje despues del salto

  // GOOMBA
  setTimeout('$("#goomba").css("animation", "desaparecer_goomba 0.5s , flash 0.5s ")', 8000) // // Le añado la animacion de desaparecer al goomba cuando baja mario del salto
  setTimeout('$("#goomba").css("opacity", "0")', 8000) // Le hago desaparecer permanentemente
  setTimeout('$("#goomba").css("margin-left", "2200px")', 8000) // Lo dejo en esta posicion fijo para que cuando acabe la animacion de moverse no se vaya a la posicion inicial

  //MARIO
  setTimeout('$("#imagen_mario").css("animation", "moverse 0.5s steps(5) infinite, andar_final 3s steps(1000) 1")', 9000);  // Hago que el mario se vaya de la pantalla
  setTimeout('$("#imagen_mario").css("opacity", "0")', 12000)  // Le quito la opacity al mario cuando acaba su animacion

  // Hago visible la animacion de cuando ya ha acabado
  setTimeout('$("#final").css("opacity", "1")', 12000)
  setTimeout('$("#final_title").css("animation", "animacion_titulo 1s both")', 12000)
  setTimeout('$(".botones_final").css("animation", "animacion_buttons 1s both")', 12000)

  // Cunado clickas a los botones final
  $(".botones_final").click(function(){
    var value = $(this).val();

    // Si le das al boton de ver de nuevo se recarga la página
    if (value == "ver_de_nuevo") {
      location.reload();
    }

    // Si le damos a jugar
    else if (value == "jugar") {
      jugar() // Llamo a la funcion jugar
    }

  });

  // Funcion para jugar
  function jugar(){
      // Hago invisible la animacion del final del juego y les añado las animaciones de salir
      $("#final_title").css("animation", "animacion_titulo_exit 1.5s both")
      $(".botones_final").css("animation", "animacion_buttons_exit 1.5s both")
      setTimeout('$("#final").css("opacity", "0")', 1500)

      // Configuro el mario al inicio de la pantalla
      $("#imagen_mario").css("margin-left", "0px");
      $("#imagen_mario").css("animation", "0");  // Le quito las animaciones para que se quede quieto
      $("#imagen_mario").css("opacity", "1");

      // Añado la imagen de los controles
      $("#controles").css("display", "block");

      // Funcion que lee cuando se pulsa una tecla
      $(document).on('keypress', function(key) {
        // -----------------------------VARIABLES ------------------------------------------//
        var dx = 80; // Variable para que se sume o se reste 50 a la positcion left/right
        var position_left = $("#imagen_mario").position().left; // Obtengo la posicion actual de imagen_mario en la variable position_left
        var position_bottom = $("#imagen_mario").position().top; // Obtengo la posicion de mario del salto
        var posicion_tubo1 = 880; // Esta es la posicion en la que se encuentra el tubo1 segun la imagen del mario
        var posicion_tubo2 = 1680; // Esta es la posicion en la que se encuentra el tubo2 segun la imagen del mario

        // Le pongo la animacion de movimiento al mario cuando se toca cualquier tecla
        $("#imagen_mario").css("animation", "moverse 0.5s steps(5) infinite");

        // Tecla W
        if (key.which == "119") {
          // Si esta en la misma posicion que el tubo 1 y salta
          if (position_left == posicion_tubo1) {
            $("#imagen_mario").css("animation", "0");
            $("#imagen_mario").css("animation", "mario_up_tubo1 1s, mario_left_tubo1 0.5s 1s, moverse 0.5s 1.5s steps(5) infinite");  // Le añado la animacion  del salto cuando esta cerca del tubo 1 y que se posicione encima del tubo1
            $("#imagen_mario").css("bottom", "324px");  // Lo dejo en esta posicion fija
            setTimeout('$("#imagen_mario").css("left", "980px");', 1500);  // Lo dejo en la posicion de left de encima del tubo 1 despues de la animacion de saltar y left
          }

          // Si el mario esta encima del tubo 1 y se le da a saltar
          else if (position_left == "980") {
            // Le añado una animacion de salto y lo pongo encima del tubo 1
            $("#imagen_mario").css("animation", "mario_up2_tubo1 0.5s, mario_left2_tubo1 0.5s 0.5s");
            setTimeout('$("#imagen_mario").css("bottom", "424px");', 500)
            setTimeout('$("#imagen_mario").css("left", "1120px");', 1000)
          }

          // Cuando le das a la W cuando el mario esta encima del todo del tubo 1
          else if (position_left == "1120"){
            console.log(position_bottom);
          }

          // Si salta sin estar a lado de ningun tubo
          else {
            $("#imagen_mario").css("animation", "0"); // Le quito la animacion para saltar
            $("#imagen_mario").css("animation", "mario_up 1s 1"); // Le hago la animacion de saltar
            setTimeout('$("#imagen_mario").css("animation", "moverse 0.5s steps(5) infinite")', 1000); // Le pongo la animacion de movimiento despues del salto
          }
        }


          // Tecla D
          else if (key.which == "100"){
            console.log(position_left);
            console.log(position_bottom);
            // Cuando el mario se cocha con el tubo 1
            if (position_left == posicion_tubo1) {
              console.log("Chocaste con el primer tubo bro");
            }

            // Si esta encima del tubo 1 y va hacia alante, no hace nada
            else if (position_left == "980"){
              console.log("Estas arriba del tubo 1 y has chocado, salta!");
            }

            // Cuando le das a la D cuando el mario esta encima del todo del tubo 1
            else if (position_left == "1120"){
              console.log(position_bottom);
            }

            // Si el mario no se cocha con el tubo, avanza hacia delante
            else {
              $("#imagen_mario").css("left", position_left + dx); // Le sumo la posicion de dx al left del mario
            }
          }

          // Tecla A
          else if (key.which == "97"){
            // Si esta encima del tubo y le damos hacia atras, añado una animacion de bajar al mario hasta el suelo
            if (position_left == "980"){
              $("#imagen_mario").css("left", position_left - dx); // Muevo atras el mario
              $("#imagen_mario").css("animation" , "mario_down_tubo1 1s, moverse 0.5s 1s steps(5) infinite"); // Le añado la animacion de bajar
              $("#imagen_mario").css("bottom" , "224px"); // Le dejo la poscicion de bottom del suelo fija
              $("#imagen_mario").css("left", posicion_tubo1); // Le pongo el left del tubo1 para que se choque y no pueda ir hacia adelante
            }

            // Cuando le das a la A cuando el mario esta encima del todo del tubo 1
            else if (position_left == "1120"){
              console.log(position_bottom);
            }

            else {
            $("#imagen_mario").css("left", position_left - dx);  // Le resto la posicion de dx a izquierda
            }
          }

          // Tecla S
          else if (key.which == "115") {
            // Si esta encima del tubo 1 y le das a S
            if (position_left == "1120") {
              $("#imagen_mario").css("animation", "mario_down2_tubo1 1s");  // Le añado la animacion de meterse en el tubo
              setTimeout('$("#imagen_mario").css("opacity", "0");', 1000)  // Le quito la opacidad

              // TEXTO FINAL
              $("#finish").css("animation", "animacion_final 1s 1s");
              setTimeout('$("#finish").css("display", "block");', 3000)

            }

            else {
              console.log("Pulsa la S cuando estes encima del tubo 1");
            }
          }


      }); // Cierro la funcion de pulsar cualquier tecla
  };  // Cierro funcion jugar

});
