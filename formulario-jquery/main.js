console.log("Welcome to jQuery.");

$(document).ready(function(){

  // Resetear los valores del formulario al cargar la pagina
  $("#form-title").val("");
  $("#form-description").val("");
  $("#form-url").val("");

  $("#new-article").click(function(){

    // Defino las variables
    var titulo = "";
    var contenido = "";
    var url = "";

    // A la variable titlo le hago un \n y le añado el value de form-title
    titulo += "<h2>";
    titulo += $("#form-title").val();
    titulo += "</h2>";

    // A la variable contenido le pongo el texto del form-description value
    contenido += "<p>";
    contenido += $("#form-description").val();
    contenido += "</p>";

    // Defino una variable que es el url introducido en el form-url
    var url0 = $("#form-url").val();
    url += "<p>"
    url += "<img src='"+ url0 +"'></img>";  // A esta variable url le asigno el formato de una imagen mas la url0 que es lo que introduce el usuario
    url += "</p>"

    // Le añado a cada id la variable creada anteriormente
    $("#titulo").append(titulo);
    $("#content").append(contenido);
    $("#url").append(url);

    // Resetear los valores del formulario
    $("#form-title").val("");
    $("#form-description").val("");
    $("#form-url").val("");

    // Para evitar que la página se recargue;
    return false;

  });

  // Funcion para resetear el contenido de las div
  $("#reset").click(function() {
    $("#form-title").val("");
    $("#form-description").val("");
    $("#form-url").val("");

    $("#titulo").append(titulo);
    $("#content").append(contenido);
    $("#url").append(url);

    return false;

  });

});
