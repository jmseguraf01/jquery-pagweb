$(document).ready(function(){  // Para llamar a Jquery

// Bucle que se ejecuta 10 veces las columnas
for (var i=0; i<10; i++) {
  var numero = 1; // Variable del numero
  for (j = 0; j<10; j++) {  // Otro blucle para que se hagan 10 celdas por columnas
    $(".container").append('<div class="l1">'+numero+'</div>');
    var numero = numero+1; // Le sumo 1 al la variable 1
  }
}


// Para que se cambie el color cuando se da encima de una celda
// $(".l1").click(function(){
//   if ($(this).css("background-color") == 'rgb(0,0,0)') {
//       $(this).css("background-color", "white");
//   }
//
//   else {
//     $(this).css("background-color", "rgb(0,0,0)");
//   }
//
// });


// Para que se muestre una paleta
$(".l1").click(function(){
  $(this).append($("#paleta"));  // Coge el div palette y lo añade al div clicado
  $("#paleta").css("display", "block")

});

// Cuando se hace click se ejecuta la siguiente funcion
$(".colors").click(function(){
  $("#paleta").parent().css("background-color", $(this).css("background-color"));  // A la div padre de paleta, se le ejecuta el background-color de .colors. (this)



});


}); // Cierro el jquery
